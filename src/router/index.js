import Vue from 'vue'
import Router from 'vue-router'
import ItemDetails from '../components/views/ItemDetails'
import Test from '../components/views/Test'
import Inventory from '../components/views/Inventory'

Vue.use(Router)

export const routes = [
  {
    path: '',
    component: Inventory
  },
  {
    path: '/item/:id',
    component: ItemDetails
  }
]
